const express = require("express");
const PORT = 8000;
const app = express();
const user = require("./modules/user");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const onRequest = (req, res) => {
  res.status(200);
  res.send("Hello from server");
};

const onNotFound = (req, res) => {
  res.status(404).send("URL Not Found!");
};

const validation = (req, res, next) => {
  const { name } = req.query;
  if (name) next();
  else res.status(400).json({ msg: "Name is required!" });
};

const newValidation = (req, res, next) => {
  const { name, age } = req.body;
  if (name && age > 20) next();
  else res.status(400).json({ msg: "Name and Age is required!" });
};

app.get("/introduce", onRequest);
app.get("/users", user.getUsers);
app.get("/user", validation, user.getUserQuery);
app.get("/user/:name", user.getUserParam);
app.post("/user", newValidation, user.createUser);
app.patch("/user", newValidation, user.createUser);

app.use(onNotFound);

app.listen(PORT, () => {
  console.log("Server is running on http://localhost:8000");
});
