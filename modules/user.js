const data = require("../dummy");

const getUsers = (req, res) => {
  res.status(200).json(data);
};

const getUserQuery = (req, res) => {
  const name = req.query.name;

  const filterdData = data.find((row) => row.name === name);

  res.status(200).json(filterdData);
};

const getUserParam = (req, res) => {
  const name = req.params.name;

  const filterdData = data.find((row) => row.name === name);

  res.status(200).json(filterdData);
};

const createUser = (req, res) => {
  data.push(req.body);
  res.status(201).json({ msg: "Data Created", data: req.body });
};

module.exports = { getUsers, getUserParam, getUserQuery, createUser };
