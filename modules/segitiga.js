const luas = (a, t) => {
  return (a * t) / 2;
};

const keliling = (s) => {
  return s * 3;
};

// module.exports = { luas, keliling };
module.exports = luas;
