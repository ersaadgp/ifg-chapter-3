const data = [
  { id: '1',name: "Harry Potter", year: 2003, publisher: "Gramedia" },
  { id: '2',name: "Spiderman", year: 2003, publisher: "AADC" },
  { id: '3',name: "Aqua Man", year: 2003, publisher: "Gunung Agung" },
  { id: '4',name: "Percy Jackson", year: 2004, publisher: "Gramedia" },
  { id: '5',name: "Batman Begins", year: 2004, publisher: "Gramedia" },
  { id: '6',name: "Negeri 3 Menara", year: 2004, publisher: "AADC" },
];

module.exports = data;
